<?php
/**
 * Translations for BHS
 *
 * @author: Marko Mikulic
 */

return [
	'Register'                                         => 'Registracija',
	'Login'                                            => 'Prijava',
	'Logout'                                           => 'Odjava',
	'Language'                                         => 'Jezik',
	'Home'                                             => 'Početna',
	'Welcome to '                                      => 'Dobrodošli na ',
	'Welcome, {full_name}'                             => 'Dobrodošli, {full_name}',
	'Please fill out the following fields to login'    => 'Molimo ispunite sljedeća polja za prijavu',
	'Please fill out the following fields to register' => 'Molimo ispunite sljedeća polja za registraciju',
	'Password'                                         => 'Lozinka',
	'First name'                                       => 'Ime',
	'Last name'                                        => 'Prezime',
	'Address'                                          => 'Adresa',
	'Tax ID'                                           => 'TAX ID',
	'Tax office'                                       => 'Tax ured',
	'Phone number'                                     => 'Broj mobitela',
	'{attribute} is required'                          => '{attribute} ne smije biti prazno',
	'{attribute_f} is required'                        => '{attribute_f} ne smije biti prazna',
	'Generate invoice'                                 => 'Kreiraj predračun',
	'To company'                                       => 'Za tvrtku',
	'Invoice'                                          => 'Predračun',
	'Invoice number'                                   => 'Broj predračuna',
	'Invoice date'                                     => 'Datum predračuna',
	'From name'                                        => 'Od',
	'Name'                                             => 'Ime',
	'Bank'                                             => 'Banka',
	'Account owner'                                    => 'Vlasnik računa',
	'Create invoice'                                   => 'Kreiraj predračun'
];