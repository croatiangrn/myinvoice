<?php
/**
 * @author: Marko Mikulic
 */

namespace app\controllers;


use app\models\InvoiceForm;
use kartik\mpdf\Pdf;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;

class InvoiceController extends Controller
{
	public function beforeAction($action)
	{
		if (!parent::beforeAction($action)) {
			return false;
		}
		$cookies = Yii::$app->request->cookies;
		// get the "language" cookie value. If the cookie does not exist, return "en" as the default value.
		$locale = $cookies->getValue('locale', 'en');
		Yii::$app->language = $locale;

		return true;
	}
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['view'],
				'rules' => [
					[
						'actions' => ['view'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	public function actionView()
	{
		$model = new InvoiceForm();
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			Yii::$app->view->title = 'Predračun br #' . $model->invoice_number;
			$response = Yii::$app->getResponse();
			$response->headers->set('Content-Type', 'application/pdf');
			$response->format = Response::FORMAT_RAW;

			// get your HTML raw content without any layouts or scripts
			$content = $this->renderPartial('_invoice-template', [
				'model' => $model
			]);

			// setup kartik\mpdf\Pdf component
			$pdf = new Pdf([
				// set to use core fonts only
				'mode' => Pdf::MODE_UTF8,
				// A4 paper format
				'format' => Pdf::FORMAT_A4,
				// portrait orientation
				'orientation' => Pdf::ORIENT_PORTRAIT,
				// stream to browser inline
				'destination' => Pdf::DEST_BROWSER,
				// your html content input
				'content' => $content,
				// format content from your own css file if needed or use the
				// enhanced bootstrap css built by Krajee for mPDF formatting
				'cssFile' => '@webroot/css/print.css',
				'options' => ['title' => 'Krajee Report Title'],
				// call mPDF methods on the fly
			]);

			// return the pdf output as per the destination setting
			return $pdf->render();
		}
		return $this->redirect(['/site/generate-invoice']);
	}

	public function actionTestView()
	{
		$model = new InvoiceForm();
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$response = Yii::$app->getResponse();
			$response->headers->set('Content-Type', 'application/pdf');
			$response->format = Response::FORMAT_RAW;

			// get your HTML raw content without any layouts or scripts
			$content = $this->renderPartial('_invoice-template', [
				'model' => $model
			]);

			// setup kartik\mpdf\Pdf component
			$pdf = new Pdf([
				// set to use core fonts only
				'mode' => Pdf::MODE_UTF8,
				// A4 paper format
				'format' => Pdf::FORMAT_A4,
				// portrait orientation
				'orientation' => Pdf::ORIENT_PORTRAIT,
				// stream to browser inline
				'destination' => Pdf::DEST_BROWSER,
				// your html content input
				'content' => $content,
				// format content from your own css file if needed or use the
				// enhanced bootstrap css built by Krajee for mPDF formatting
				'cssFile' => '@webroot/css/print.css',
				'options' => ['title' => 'Krajee Report Title'],
				// call mPDF methods on the fly
			]);

			// return the pdf output as per the destination setting
			return $pdf->render();
		}
	}
}