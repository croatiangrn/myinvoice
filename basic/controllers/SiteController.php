<?php

namespace app\controllers;

use app\models\BankDetails;
use app\models\InvoiceForm;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Cookie;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;

class SiteController extends Controller
{
	public function beforeAction($action)
	{
		if (!parent::beforeAction($action)) {
			return false;
		}
		$cookies = Yii::$app->request->cookies;
		// get the "language" cookie value. If the cookie does not exist, return "en" as the default value.
		$locale = $cookies->getValue('locale', 'en');
		Yii::$app->language = $locale;

		return true;
	}

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'generate-invoice'],
                'rules' => [
                    [
                        'actions' => ['logout', 'generate-invoice'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                /*'actions' => [
                    'logout' => ['post'],
                ],*/
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

	/**
	 * Register action
	 *
	 * @return string|Response
	 * @throws \yii\base\Exception
	 */
	public function actionRegister()
	{
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new User();
		$bankDetailsModel = new BankDetails();
		if ($model->load(Yii::$app->request->post()) && $bankDetailsModel->load(Yii::$app->request->post())) {
			$isValid = $model->validate();
			$isValid = $bankDetailsModel->validate() && $isValid;
			if ($isValid) {
				// Let's encrypt password
				$model->setPassword($model->password);
				$model->save();
				$model->refresh();

				$bankDetailsModel->user_id = $model->id;
				$bankDetailsModel->save();
				Yii::$app->getSession()->setFlash( 'registration_success', "You have been registered successfully!");
				return $this->goHome();
			}
		}

		return $this->render('register', [
			'model' => $model,
			'bankDetailsModel' => $bankDetailsModel
		]);
	}

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

	/**
	 * Language switcher action
	 *
	 * @param $locale string Language code (IS0-639-1)
	 *
	 * @see https://en.wikipedia.org/wiki/ISO_639-1 List of available codes
	 *
	 * @return Response
	 */
	public function actionLanguage($locale)
	{
		$allowedLanguages = ['en', 'bs'];

		if (!in_array($locale, $allowedLanguages)) {
			$locale = 'en';
		}

		Yii::$app->language = $locale;

		$languageCookie = new Cookie([
			'name' => 'locale',
			'value' => $locale,
			'expire' => time() + 60 * 60 * 24 * 30, // 30 days
		]);
		Yii::$app->response->cookies->add($languageCookie);

		return $this->goBack();
    }

	public function actionGenerateInvoice()
	{
		$model = new InvoiceForm();
		return $this->render('generate-invoice',[
			'model' => $model
		]);
    }
}
