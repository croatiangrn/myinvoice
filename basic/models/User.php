<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property string $first_name
 * @property string $last_name
 * @property string $address
 * @property string $email
 * @property string $tax_id
 * @property string $tax_office
 * @property string $phone_number
 * @property string $password
 * @property string $password_reset_token
 * @property string $auth_key
 */
class User extends ActiveRecord implements IdentityInterface
{
	public $full_name;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['email', 'tax_id', 'tax_office', 'phone_number', 'password'], 'required'],
            [['first_name', 'last_name'], 'required', 'message' => Yii::t('app', '{attribute} is required')],
            [['address'], 'required', 'message' => Yii::t('app', '{attribute_f} is required', ['attribute_f' => '{attribute}'])],
	        [['first_name', 'last_name'], 'string', 'max' => 30],
            [['address'], 'string', 'max' => 250],
            [['email', 'tax_id', 'tax_office'], 'string', 'max' => 100],
            [['phone_number'], 'number'],
            [['password'], 'string', 'length' => [8,255]],
	        [['email', 'phone_number'], 'unique'],
	        ['email', 'email'],
        ];
    }

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'           => 'ID',
			'created_at'   => 'Created At',
			'updated_at'   => 'Updated At',
			'deleted_at'   => 'Deleted At',
			'first_name'   => Yii::t('app','First name'),
			'last_name'    => Yii::t('app','Last name'),
			'address'      => Yii::t('app','Address'),
			'email'        => Yii::t('app','Email'),
			'tax_id'       => Yii::t('app','Tax ID'),
			'tax_office'   => Yii::t('app','Tax office'),
			'phone_number' => Yii::t('app','Phone number'),
			'password'     => Yii::t('app','Password'),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}

	/**
	 * Get user's full name
	 *
	 * @return string
	 */
	public function getFullName()
	{
		return $this->first_name . ' ' . $this->last_name;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBankDetails()
	{
		return $this->hasMany(BankDetails::className(), ['user_id' => 'id']);
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentity($id)
	{
		return static::findOne(['id' => $id, 'deleted_at' => null]);
	}
	/**
	 * @inheritdoc
	 */
	public static function findIdentityByAccessToken($token, $type = null)
	{
		throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
	}
	/**
	 * Finds user by username
	 *
	 * @param string $email
	 * @return static|null
	 */
	public static function findByEmail($email)
	{
		return static::findOne(['email' => $email, 'deleted_at' => null]);
	}
	/**
	 * Finds user by password reset token
	 *
	 * @param string $token password reset token
	 * @return static|null
	 */
	public static function findByPasswordResetToken($token)
	{
		if (!static::isPasswordResetTokenValid($token)) {
			return null;
		}
		return static::findOne([
			'password_reset_token' => $token,
			'deleted_at' => null,
		]);
	}
	/**
	 * Finds out if password reset token is valid
	 *
	 * @param string $token password reset token
	 * @return bool
	 */
	public static function isPasswordResetTokenValid($token)
	{
		if (empty($token)) {
			return false;
		}
		$timestamp = (int) substr($token, strrpos($token, '_') + 1);
		$expire = Yii::$app->params['user.passwordResetTokenExpire'];
		return $timestamp + $expire >= time();
	}
	/**
	 * @inheritdoc
	 */
	public function getId()
	{
		return $this->getPrimaryKey();
	}
	/**
	 * @inheritdoc
	 */
	public function getAuthKey()
	{
		return $this->auth_key;
	}
	/**
	 * @inheritdoc
	 */
	public function validateAuthKey($authKey)
	{
		return $this->getAuthKey() === $authKey;
	}
	/**
	 * Validates password
	 *
	 * @param string $password password to validate
	 * @return bool if password provided is valid for current user
	 */
	public function validatePassword($password)
	{
		return Yii::$app->security->validatePassword($password, $this->password);
	}

	/**
	 * Generates password hash from password and sets it to the model
	 *
	 * @param string $password
	 *
	 * @throws \yii\base\Exception
	 */
	public function setPassword($password)
	{
		$this->password = Yii::$app->security->generatePasswordHash($password);
	}
	/**
	 * Generates "remember me" authentication key
	 */
	public function generateAuthKey()
	{
		$this->auth_key = Yii::$app->security->generateRandomString();
	}
	/**
	 * Generates new password reset token
	 */
	public function generatePasswordResetToken()
	{
		$this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
	}
	/**
	 * Removes password reset token
	 */
	public function removePasswordResetToken()
	{
		$this->password_reset_token = null;
	}
}
