<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "bank_details".
 *
 * @property int $id
 * @property int $user_id
 * @property string $bank
 * @property string $swift
 * @property string $iban
 *
 * @property User $user
 */
class BankDetails extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bank_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bank', 'swift', 'iban'], 'required'],
            [['user_id'], 'integer'],
            [['bank'], 'string', 'max' => 255],
            [['swift', 'iban'], 'string', 'max' => 70],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
	    return [
		    'id'      => Yii::t( 'app', 'ID' ),
		    'user_id' => Yii::t( 'app', 'User ID' ),
		    'bank'    => Yii::t( 'app', 'Bank' ),
		    'swift'   => Yii::t( 'app', 'Swift' ),
		    'iban'    => Yii::t( 'app', 'Iban' ),
	    ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
