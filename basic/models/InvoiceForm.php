<?php
/**
 * @author: Marko Mikulic
 */

namespace app\models;


use Yii;
use yii\base\Model;

class InvoiceForm extends Model
{
	public $toCompany;
	public $address;
	public $invoice;
	public $invoice_number;
	public $invoice_date;

	public function rules()
	{
		return [
			[['toCompany', 'address', 'invoice', 'invoice_number', 'invoice_date'], 'required']
		];
	}

	public function attributeLabels()
	{
		return [
			'toCompany' => Yii::t( 'app', 'To company' ),
			'address'   => Yii::t( 'app', 'Address' ),
			'invoice_number' => Yii::t('app', 'Invoice number'),
			'invoice' => Yii::t('app', 'Invoice'),
			'invoice_date' => Yii::t('app', 'Invoice date'),
		];
	}
}