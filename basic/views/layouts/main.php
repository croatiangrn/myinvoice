<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\components\Greeter;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) . ' - ' . Yii::$app->name ?></title>
    <?php $this->head() ?>
</head>
<body class="Site">
<?php $this->beginBody() ?>

<div class="wrap Site-content">
    <?php
    $isGuest = Yii::$app->user->isGuest;

    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            // Unauthorized links
	        ['label' => Yii::t('app', 'Register'), 'url' => ['/site/register'], 'visible' => $isGuest],
            ['label' => Yii::t('app','Login'), 'url' => ['/site/login'], 'visible' => $isGuest],
	        // Authorized links
            ['label' => ( !$isGuest ) ? Greeter::getGreetingMessage() : null, 'visible' => ! $isGuest],
            ['label' => Yii::t('app','Generate invoice'), 'url' => ['/site/generate-invoice'], 'visible' => ! $isGuest],
            ['label' => Yii::t('app','Logout'), 'url' => ['/site/logout'], 'visible' => ! $isGuest],
            ['label' => Yii::t('app', 'Language'), 'items' => [
	            ['label' => 'ENG', 'url' => ['/site/language', 'locale' => 'en']],
                ['label' => 'BHS', 'url' => ['/site/language', 'locale' => 'bs']],
            ]]
        ],
    ]);
    NavBar::end();
    ?>


    <?php
    $currentRoute = Yii::$app->controller->action->id;
    $containerClass = ($currentRoute == 'index') ? ' background-img h-100p' : '';
    ?>

    <div class="container full-width <?= $containerClass ?>">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container text-center">
        <p>Copyright <?= Yii::$app->name . ' ' . date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
