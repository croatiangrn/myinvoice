<?php

/* @var $this yii\web\View */

use app\components\Greeter;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Home');
?>
<div class="site-index">

    <div class="jumbotron">

        <h1>
            <?php
            if (Yii::$app->user->isGuest) {
                echo Yii::t('app', 'Welcome to ') . Yii::$app->name;
            } else {
	            echo Greeter::getGreetingMessage();
            }
            ?>
        </h1>
    </div>

    <div class="body-content">

        <div class="container">
            <div class="row">
                <div class="col-lg-6 text-center">
                    <h2><?= Yii::t('app', 'Register') ?></h2>
                    <p>
                        <?= Html::a(Yii::t('app', 'Register'), ['/site/register'], ['class' => 'btn btn-default']) ?>
                    </p>
                </div>
                <div class="col-lg-6 text-center">
                    <h2><?= Yii::t('app', 'Login') ?></h2>
                    <p>
		                <?= Html::a(Yii::t('app', 'Login'), ['/site/login'], ['class' => 'btn btn-default']) ?>
                    </p>
                </div>
            </div>
        </div>

    </div>
</div>
