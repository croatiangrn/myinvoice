<?php
/**
 * @author: Marko Mikulic
 */

use dosamigos\tinymce\TinyMce;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Generate invoice');

$user = Yii::$app->user->identity;
$userBankDetails = $user->getBankDetails()->one();
?>
<div class="container">

	<div class="row">
		<?php
		$form = ActiveForm::begin([
            'id' => 'invoice-form',
            'action' => ['/invoice/view'],
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-6\">{input}</div>\n<div class=\"col-lg-4\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-2 control-label text-left'],
            ],
        ]); ?>

        <div class="col-md-12 text-center">
           <h2>NAME</h2>
        </div>

		<div class="col-md-6">
			<?= $form->field($model, 'toCompany')->textInput(['autofocus' => true]) ?>
			<?= $form->field($model, 'address')->textInput() ?>
			<?= $form->field($model, 'invoice_number')->textInput() ?>
			<?= $form->field($model, 'invoice_date')->widget(\yii\jui\DatePicker::className(), [
				'language' => Yii::$app->language,
				'dateFormat' => 'dd-MM-yyyy',
				'options' => ['class' => 'form-control']
			]) ?>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="col-lg-2 control-label text-left">From:</label>
                <div class="col-lg-6"><span class="form-control"><?= $user->fullName ?></span></div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label text-left"><?= Yii::t('app', 'Address') ?>:</label>
                <div class="col-lg-6"><span class="form-control"><?= $user->address ?></span></div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label text-left"><?= Yii::t('app', 'Phone') ?>:</label>
                <div class="col-lg-6"><span class="form-control"><?= $user->phone_number ?></span></div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label text-left"><?= Yii::t('app', 'Email') ?>:</label>
                <div class="col-lg-6"><span class="form-control"><?= $user->email ?></span></div>
            </div>

            <br>
            <br>

            <div class="form-group">
                <label class="col-lg-2 control-label text-left"><?= Yii::t('app', 'Tax ID') ?>:</label>
                <div class="col-lg-6"><span class="form-control"><?= $user->tax_id ?></span></div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label text-left"><?= Yii::t('app', 'Tax office') ?>:</label>
                <div class="col-lg-6"><span class="form-control"><?= $user->tax_office ?></span></div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label text-left"><?= Yii::t('app', 'Bank') ?>:</label>
                <div class="col-lg-6"><span class="form-control"><?= $userBankDetails->bank ?></span></div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label text-left"><?= Yii::t('app', 'Account owner') ?>:</label>
                <div class="col-lg-6"><span class="form-control"><?= $user->fullName ?></span></div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label text-left"><?= Yii::t('app', 'IBAN') ?>:</label>
                <div class="col-lg-6"><span class="form-control"><?= $userBankDetails->iban ?></span></div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label text-left"><?= Yii::t('app', 'SWIFT') ?>:</label>
                <div class="col-lg-6"><span class="form-control"><?= $userBankDetails->swift ?></span></div>
            </div>
        </div>


        <div class="col-md-12">
            <h2 class="text-center text-uppercase"><?= Yii::t('app', 'Invoice') ?></h2>
        </div>

		<?= $form->field($model, 'invoice', [
			'template' => "{label}\n<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
			'labelOptions' => ['class' => 'col-lg-1 control-label text-left'],
		])->widget(TinyMce::className(), [
			'options' => ['rows' => 12],
			'language' => Yii::$app->language,
			'clientOptions' => [
				'plugins' => [
					"advlist autolink lists link charmap print preview anchor",
					"searchreplace visualblocks code fullscreen",
					"insertdatetime media table contextmenu paste image"
				],
				'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
			]
		]);?>


		<div class="form-group">
			<div class="col-lg-offset-1 col-lg-11">
				<?= Html::submitButton(Yii::t('app', 'Create invoice'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
			</div>
		</div>


		<?php ActiveForm::end(); ?>
	</div>

</div>
