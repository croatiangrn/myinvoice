<?php
/**
 * @author: Marko Mikulic
 */
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */
/* @var $bankDetailsModel \app\models\BankDetails */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Register');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= Yii::t('app', 'Please fill out the following fields to register') ?>:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-4\">{input}</div>\n<div class=\"col-lg-6\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
    ]); ?>

        <?= $form->field( $model, 'first_name' )->textInput( [ 'autofocus' => true ] ) ?>
        <?= $form->field( $model, 'last_name' )->textInput() ?>
        <?= $form->field( $model, 'email' )->textInput() ?>
        <?= $form->field( $model, 'address' )->textInput() ?>
        <?= $form->field( $model, 'tax_id' )->textInput() ?>
        <?= $form->field( $model, 'tax_office' )->textInput() ?>
        <?= $form->field( $model, 'phone_number' )->textInput() ?>
        <?= $form->field( $bankDetailsModel, 'bank' )->textInput() ?>
        <?= $form->field( $bankDetailsModel, 'swift' )->textInput() ?>
        <?= $form->field( $bankDetailsModel, 'iban' )->textInput() ?>
        <?= $form->field( $model, 'password' )->passwordInput() ?>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton(Yii::t('app', 'Register'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
</div>
