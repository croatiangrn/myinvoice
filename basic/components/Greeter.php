<?php
/**
 * @author: Marko Mikulic
 */

namespace app\components;


use Yii;

class Greeter
{
	/**
	 * Returns internationalized greeting message
	 * This method is used in header and on homepage
	 *
	 * @return string
	 */
	public static function getGreetingMessage()
	{
		return Yii::t('app', 'Welcome, {full_name}', [
			'full_name' => Yii::$app->user->identity->fullName
		]);
	}
}